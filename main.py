import argparse
import getpass
from imap_session import ImapSession


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', default='imap.mail.ru',
                        help='IMAP server address.')
    parser.add_argument('--ssl', action='store_const', const=True,
                        default=False, help='Need ssl connection?')
    parser.add_argument('-u', '--user', default=None,
                        help='User name')

    # TODO: -n N1 [N2]: N1 - обязательный, N2 - необязательный
    parser.add_argument('-n',
                        nargs=2, help='Range of letters', type=int,
                        default=[None, None])
    return parser


def get_args():
    args = get_parser().parse_args()

    user = args.user
    args.passwd = getpass.getpass('Input your password: ') if user else None

    return args


def main():
    args = get_args()

    i_session = ImapSession(args.server, args.ssl, args.user, args.passwd)
    letters = i_session.get_letter_range(*args.n)


if __name__ == '__main__':
    main()
